FROM php:8.0-apache
RUN docker-php-ext-install pdo pdo_mysql
RUN a2enmod rewrite ssl headers proxy proxy_http

LABEL name="OpenSV php image" image="opensv\php"

RUN pecl install xdebug-3.0.2 \
    && docker-php-ext-enable xdebug

# Install composer and update path
ENV COMPOSER_HOME /composer
ENV PATH /composer/vendor/bin:$PATH
ENV COMPOSER_ALLOW_SUPERUSER 1
RUN curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer

LABEL name="OpenSV php dev image" image="opensv\php-dev"