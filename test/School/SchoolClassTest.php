<?php

namespace Test\School;

use OpenSV\School\SchoolClass;
use PHPUnit\Framework\TestCase;

class SchoolClassTest extends TestCase
{
    /**
     * @dataProvider schoolClassNames
     */
    public function testSchoolClassHasAName(string $name): void
    {
        $class = new SchoolClass($name);
        self::assertEquals($name, $class->Name());
    }

    public function schoolClassNames(): array
    {
        return [['1A'], ['2A'], ['1B'], ['2B']];
    }

}
