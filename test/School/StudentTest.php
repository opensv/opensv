<?php

namespace Test\School;

use OpenSV\School\SchoolClass;
use OpenSV\School\Student;
use PHPUnit\Framework\TestCase;

class StudentTest extends TestCase
{
    /**
     * @dataProvider studentNames
     * @test
     */
    public function studentHasAName(string $name): void
    {
        $student = new Student($name);
        self::assertEquals($name, $student->Name());
    }

    public function studentNames(): array
    {
        return [['Ada'], ['Peter'], ['Simon']];
    }

    /**
     * @test
     */
    public function studentKnowsTheClass(): void
    {
        $student = new Student('Ada');
        $class   = new SchoolClass('1A');
        $returnsSelf = $student->assignToClass($class);
        self::assertSame($class, $student->class());
        self::assertSame($student, $returnsSelf, 'assigning a class to a student should return the student');
    }
}
