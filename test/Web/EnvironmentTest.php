<?php

namespace Test\Web;

use OpenSV\Web\Environment;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Slim\App;
use Slim\Psr7\Request;

class EnvironmentTest extends TestCase
{
    /**
     * @test
     */
    public function respondWillCreateAResponse(): void
    {
        $slimMock = $this->createMock(App::class);
        $request = $this->createMock(Request::class);
        $response = $this->createMock(ResponseInterface::class);

        // set status code of response to 204 to ensure the response will be interpreted as "empty"
        $response->method('getStatusCode')->willReturn(204);
        $response->method('withHeader')->willReturn($response);
        $response->method('withAddedHeader')->willReturn($response);

        $slimMock
            ->expects(self::once())
            ->method('handle')
            ->with($request)
            ->willReturn($response);
        $environment = new Environment($slimMock);
        $environment->respond($request);
    }
}
