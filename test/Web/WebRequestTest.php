<?php

namespace Test\Web;

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class WebRequestTest extends TestCase
{
    /**
     * @test
     */
    public function aRequestWillBeGetAResponse(): void
    {
        $client = new Client();
        $response = $client->request('GET', 'http://localhost:8080', ['query' => ['XDEBUG_MODE' => 'coverage']]);
        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals('', $response->getBody()->getContents());
    }

}
