<?php

namespace Test\Web\Resource;

use OpenSV\Web\Resource\Index;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\StreamInterface;

class IndexTest extends TestCase
{

    /**
     * @test
     */
    public function getWillRespondWithAMessage(): void
    {
        $index = new Index();
        $response = $this->createMock(Response::class);
        $stream = $this->createMock(StreamInterface::class);
        $response
            ->expects(self::once())
            ->method('getBody')
            ->willReturn($stream);
        $stream
            ->expects(self::once())
            ->method('write')
            ->with('');
        self::assertSame($response, $index->get($this->createMock(Request::class), $response));
    }
}
