<?php
declare(strict_types=1);

namespace OpenSV\Web\Handlers;

use Fig\Http\Message\StatusCodeInterface;
use JetBrains\PhpStorm\ArrayShape;
use OpenSV\Web\Exception\HttpException;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Handlers\ErrorHandler as SlimErrorHandler;
use Throwable;

class HttpErrorHandler extends SlimErrorHandler
{
    private bool $debug = false;

    public function debug(): self
    {
        $this->debug = !$this->debug;

        return $this;
    }

    protected function respond(): Response
    {
        $payload = $this->getExceptionPayload($this->exception);

        $encodedPayload = json_encode($payload, JSON_PRETTY_PRINT);

        $response = $this->responseFactory->createResponse($this->getStatusCode());
        $response->getBody()->write($encodedPayload);

        return $response->withHeader('Content-Type', 'application/json');
    }

    #[ArrayShape([
        'message'  => "string",
        'code'     => "int",
        'previous' => "array",
        'trace'    => "string",
        'line'     => "int",
        'file'     => "string",
    ])]
    private function getExceptionPayload(Throwable $throwable): array
    {
        $payload = [
            'message' => $throwable->getMessage(),
            'code'    => $throwable->getCode(),
        ];
        if ($this->debug) {
            $payload['file'] = $throwable->getFile();
            $payload['line'] = $throwable->getLine();
            $payload['trace'] = $throwable->getTraceAsString();
        }
        if ($throwable->getPrevious()) {
            $payload['previous'] = $this->getExceptionPayload($throwable->getPrevious());
        }
        return $payload;
    }

    private function getStatusCode(): int
    {
        if ($this->exception instanceof HttpException) {
            return $this->exception->getStatusCode();
        }
        return StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR;
    }
}
