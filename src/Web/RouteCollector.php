<?php


namespace OpenSV\Web;


use HaydenPierce\ClassFinder\ClassFinder;
use OpenSV\Web\Resource\GetResource;
use OpenSV\Web\Resource\RoutePattern;
use ReflectionClass;
use Slim\App;

class RouteCollector
{
    public function defineRoutes(App $app): void
    {
        ClassFinder::disableClassmapSupport();
        foreach (ClassFinder::getClassesInNamespace(__NAMESPACE__ . '\Resource', ClassFinder::RECURSIVE_MODE) as $class) {
            $reflector = new ReflectionClass($class);
            if (!$reflector->isInstantiable()) {
                continue;
            }
            if ($reflector->implementsInterface(GetResource::class)) {
                foreach ($reflector->getAttributes(RoutePattern::class) as $attribute) {
                    $pattern = $attribute->newInstance();
                    $app->get($pattern->getPattern(), $class . ':get');
                }
            }
        }
    }
}