<?php


namespace OpenSV\Web\Resource;


use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

#[RoutePattern('/')]
class Index implements GetResource
{
    public function get(Request $request, Response $response): Response
    {
        $response->getBody()->write('');
        return $response;
    }
}