<?php


namespace OpenSV\Web\Resource;


use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

interface GetResource
{
    public function get(Request $request, Response $response): Response;
}