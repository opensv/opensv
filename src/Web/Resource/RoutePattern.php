<?php


namespace OpenSV\Web\Resource;


use Attribute;

#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD | Attribute::TARGET_FUNCTION)]
class RoutePattern
{
    public function __construct(private string $pattern)
    {
    }

    public function getPattern(): string
    {
        return $this->pattern;
    }
}