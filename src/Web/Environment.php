<?php


namespace OpenSV\Web;


use DI\ContainerBuilder;
use JetBrains\PhpStorm\Pure;
use OpenSV\Web\Handlers\HttpErrorHandler;
use OpenSV\Web\Handlers\ShutdownHandler;
use OpenSV\Web\ResponseEmitter\ResponseEmitter;
use Slim\App;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Factory\ServerRequestCreatorFactory;
use Throwable;

class Environment
{
    private App $app;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    /**
     * @codeCoverageIgnore
     */
    public static function handleRequest(): void
    {
        $slim = self::bootstrapSlim();
        $environment = new self($slim);
        $environment->respond(self::buildRequest());
    }

    /**
     * @codeCoverageIgnore
     */
    private static function bootstrapSlim(): App
    {
        try {
            $containerBuilder = new ContainerBuilder();

            AppFactory::setContainer($containerBuilder->build());

            return AppFactory::create();
        } catch (Throwable $e) {
            die($e->getMessage());
        }
    }

    /**
     * @codeCoverageIgnore
     */
    private static function buildRequest(): Request
    {
        return ServerRequestCreatorFactory::create()->createServerRequestFromGlobals();
    }

    public function respond(Request $request): void
    {
        $this->createShutdownHandler($request);
        $this->setupMiddlewares();
        $this->defineRoutes();

        $this->emitResponse($request);
    }

    #[Pure]
    private function getHttpErrorHandler(): HttpErrorHandler
    {
        return new HttpErrorHandler($this->app->getCallableResolver(), $this->app->getResponseFactory());
}

    private function createShutdownHandler(Request $request): void
    {
        register_shutdown_function(
            new ShutdownHandler($request, $this->getHttpErrorHandler(), $this->shouldDisplayErrorDetails())
        );
    }

    private function setupMiddlewares(): void
    {
        $this->app->addRoutingMiddleware();

        $errorMiddleware = $this->app->addErrorMiddleware($this->shouldDisplayErrorDetails(), false, false);
        $errorMiddleware->setDefaultErrorHandler($this->getHttpErrorHandler());
    }

    private function defineRoutes(): void
    {
        $router = new RouteCollector();
        $router->defineRoutes($this->app);
    }

    private function shouldDisplayErrorDetails(): bool
    {
        return true;
    }

    /**
     * @param Request $request
     */
    private function emitResponse(Request $request): void
    {
        $response = $this->app->handle($request);
        $responseEmitter = new ResponseEmitter();
        $responseEmitter->emit($response);
    }
}