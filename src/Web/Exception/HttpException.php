<?php


namespace OpenSV\Web\Exception;


use Fig\Http\Message\StatusCodeInterface;

interface HttpException extends StatusCodeInterface
{
    /**
     * must return one of the constants defined in StatusCodeInterface
     */
    public function getStatusCode(): int;
}