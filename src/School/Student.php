<?php


namespace OpenSV\School;


class Student
{
    private string $name;

    private SchoolClass $class;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function Name(): string
    {
        return $this->name;
    }

    public function assignToClass(SchoolClass $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function class(): SchoolClass
    {
        return $this->class;
    }
}