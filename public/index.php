<?php
declare(strict_types=1);

use OpenSV\Web\Environment;

require dirname(realpath(__DIR__)) . '/vendor/autoload.php';

Environment::handleRequest();